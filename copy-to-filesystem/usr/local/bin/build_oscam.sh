#!/bin/bash
set -e

rm -rf /opt/oscam_build || true

mkdir -p /opt/oscam_build
cd /opt/oscam_build
git clone https://github.com/oscam-emu/oscam-patched oscam-src
cd oscam-src
make allyesconfig
make USE_SSL=1 USE_PCSC=1 USE_LIBUSB=1 OSCAM_BIN=oscam

systemctl stop oscam || true
killall oscam || true

cp ./oscam /usr/local/bin/oscam

cd
rm -rf /opt/oscam_build
